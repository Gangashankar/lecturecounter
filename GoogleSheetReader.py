import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime

#import os


# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
#os.chdir(path)
creds = ServiceAccountCredentials.from_json_keyfile_name('/Users/gangashankarsingh/PycharmProjects/PythonLectureCounter/PythonLectureCount-14c272ab5b53.json', scope)
client = gspread.authorize(creds)

sheet = client.open("Lecture Count").worksheet('Sheet1')

# for i in sheet:
#     print(i)


# Extract and print all of the values
all_records = sheet.get_all_records()
#print(type(all_records))

sub_list = ['TYIT IoT (T)','TYIT IoT (P) B1','TYIT IoT (P) B2','TYIT IoT (P) B3','SYCS IoT (T)',
            'SYCS IoT (P) B1','SYCS IoT (P) B2','SYCS IoT (P) B3','FYIT IP (T)','FYCS IP (T)']

tempplannedsum=0
tempexecutedsum=0


print("Enter start date in dd/mm/yyyy format: ")
str_start_date = input()

print("Enter end date in dd/mm/yyyy format: ")
str_end_date = input()

format_str = '%d/%m/%Y'  # The format
start_date = datetime.datetime.strptime(str_start_date, format_str)
end_date = datetime.datetime.strptime(str_end_date, format_str)


print("Start date is:" + str(start_date.date()))
print("End date is:" + str(end_date.date()))

for sub in sub_list:
    tempplannedsum = 0
    tempexecutedsum=0

    for row in all_records:

        #for col in row:
            #if col.find(sub_list[0]) > -1:
        value = row.get(sub.strip())

        date_val = row.get('Date').strip()



        #date_str = '29/12/2017'  # The date - 29 Dec 2017
        format_str = '%m/%d/%Y'  # The format
        datetime_obj = datetime.datetime.strptime(date_val, format_str)
        #print(datetime_obj.date())

        if value != '' and ( datetime_obj >= start_date  and  datetime_obj <= end_date):
            tempplannedsum = tempplannedsum + int(value.split(',')[0])
            tempexecutedsum = tempexecutedsum + int(value.split(',')[1])
    print("For " + sub +  "\n" + " Planned Lec:" + str(tempplannedsum) + "," + "Executed Lec:" + str(tempexecutedsum))
